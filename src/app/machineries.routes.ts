import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AllMachineriesComponent } from './machineries/all-machineries/all-machineries.component';
import { AddMachineryComponent } from './machineries/add-machinery/add-machinery.component';
import { MachineryComponent } from './machineries/machinery/machinery.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ViewOwnersComponent } from './owners/view-owners/view-owners.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { ViewCategoriesComponent } from './category/view-categories/view-categories.component';
import { ViewCategoryComponent } from './category/view-category/view-category.component';
import { ProfileComponent } from './owners/profile/profile.component';
import { CreateAdvertComponent } from './advert/create-advert/create-advert.component';
import { ViewAdvertComponent } from './advert/view-advert/view-advert.component';
import { ViewAdvertsComponent } from './advert/view-adverts/view-adverts.component';
import { CreateSubscriptionComponent } from './subscription/create-subscription/create-subscription.component';
import { ViewSubscriptionComponent } from './subscription/view-subscription/view-subscription.component';
import { ViewSubscriptionsComponent } from './subscription/view-subscriptions/view-subscriptions.component';
import { CreateClassifiedComponent } from './classified/create-classified/create-classified.component';
import { AllClassifiedsComponent } from './classified/all-classifieds/all-classifieds.component';
import { ClassifiedComponent } from './classified/classified/classified.component';
import { MachineryAdminGuard } from './machinery-admin.guard';


export const Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent, canActivate:[MachineryAdminGuard] },
    { path: 'all-machines', component: AllMachineriesComponent, canActivate:[MachineryAdminGuard] },
    { path: 'add-machine', component: AddMachineryComponent, canActivate:[MachineryAdminGuard] },
    { path: 'machine/:id', component: MachineryComponent, canActivate:[MachineryAdminGuard] },
    { path: 'admin-profile', component: AdminProfileComponent, canActivate:[MachineryAdminGuard] },
    { path: 'profile/:id', component: ProfileComponent, canActivate:[MachineryAdminGuard] },
    { path: 'view-owners', component: ViewOwnersComponent, canActivate:[MachineryAdminGuard] },
    { path: 'add-category', component: AddCategoryComponent, canActivate:[MachineryAdminGuard] },
    { path: 'view-category/:id', component: ViewCategoryComponent, canActivate:[MachineryAdminGuard] },       
    { path: 'view-categories', component: ViewCategoriesComponent, canActivate:[MachineryAdminGuard] },
    { path: 'create-advert', component: CreateAdvertComponent, canActivate:[MachineryAdminGuard] },    
    { path: 'view-adverts', component: ViewAdvertsComponent, canActivate:[MachineryAdminGuard] },    
    { path: 'view-advert/:id', component: ViewAdvertComponent, canActivate:[MachineryAdminGuard] },
    { path: 'create-subscription', component: CreateSubscriptionComponent, canActivate:[MachineryAdminGuard] },    
    { path: 'view-subscriptions', component: ViewSubscriptionsComponent, canActivate:[MachineryAdminGuard] },    
    { path: 'view-subscription/:id', component: ViewSubscriptionComponent, canActivate:[MachineryAdminGuard] },
    { path: 'create-classified', component: CreateClassifiedComponent, canActivate:[MachineryAdminGuard] },    
    { path: 'all-classifieds', component: AllClassifiedsComponent, canActivate:[MachineryAdminGuard] },    
    { path: 'classified/:id', component: ClassifiedComponent, canActivate:[MachineryAdminGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent, canActivate:[MachineryAdminGuard] },
    { path: '**', component: DashboardComponent }

]