import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MachineriesService } from '../machineries.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginUrl: String = 'login';
  adminDetails = {
    "data" : {
        "password": ""
    },
    "find":{
      "email": ""
    }
}


  loader: boolean= false;

  constructor(private _machineryService: MachineriesService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.loader = true;
    console.log(this.adminDetails);
    this._machineryService.postData(this.loginUrl, this.adminDetails).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        alert(res['message']);
        this.loader = false;
      } else {
        console.log(res);
        this.loader = false;        
        localStorage.setItem('admin', JSON.stringify(res));
        this.router.navigateByUrl('/dashboard');
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page');
      this.loader = false;
    });
  }

}
