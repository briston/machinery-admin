import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  inputs: ['pageTitle'],
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  pageTitle:string;
  data:string;
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  searchData(){
    localStorage.setItem('search', JSON.stringify(this.data));
    console.log(this.data);
    window.location.href = '/all-machines';
  }

}
