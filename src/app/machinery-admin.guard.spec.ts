import { TestBed, async, inject } from '@angular/core/testing';

import { MachineryAdminGuard } from './machinery-admin.guard';

describe('MachineryAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MachineryAdminGuard]
    });
  });

  it('should ...', inject([MachineryAdminGuard], (guard: MachineryAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
