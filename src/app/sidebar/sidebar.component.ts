import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  inputs: ['activePage'],
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  adminDetails;
  activePage:string;
  constructor(private router:Router) { }

  ngOnInit() {
    //this.activePage = 'dashboard';
    console.log('activePage: ', this.activePage);
    this.adminDetails = JSON.parse(localStorage.getItem('admin')).result;
    console.log(this.adminDetails);
  }

  logout(){
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
}
