import { Component, OnInit } from '@angular/core';
import { MachineriesService } from '../../machineries.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-machinery',
  templateUrl: './add-machinery.component.html',
  styleUrls: ['./add-machinery.component.css']
})
export class AddMachineryComponent implements OnInit {
  networkError: boolean = false;
  pageTitle: string;
  machineUrl: string = 'machine';
  categoriesUrl: string = 'categories';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';
  formData = new FormData();
  photoLoader = false;

  categories: any = [];

  machine = {
    'data': {
      'name': '',
      'description': '',
      'location': '',
      'category': '',
      'image': '',
      'address': '',
      'contact_phone': '',
      'contact_email': '',
      'price': '',
      "classified": false
    },
    'token': ''

  }

  admin;
  loader:boolean = false;
  activePage:string;

  constructor(private _machineryService: MachineriesService, private _http: HttpClient) { }

  ngOnInit() {
    this.pageTitle = 'Add Machine';
    this.activePage = 'add-machine';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    console.log('token',this.admin.token);
    this.getCategories();    
  }

  getCategories(){
    let token = this.admin['token'];
    this._machineryService.getData(this.categoriesUrl, `?token=${token}&page=0&order=-updated_at` ).subscribe(res=> {
      if(res['error']==true){
        console.log(res);
        // this.loader = false;
      } else {
        console.log(res);
        this.categories = res['result'];
        // this.loader = false;
      }
    }, err=>{
      alert('Please, check network connection and refresh page!')
      // this.loader = false;
      this.networkError = true;
      console.log(err);
    });
  }

  // Grab photo file
  grabPhoto = (file) => {
    this.photoLoader = true;
    // set disable to submit button on photo upload
    let attr:number;
    let submitBtn = document.getElementById('submitBtn');
    if(!submitBtn.hasAttribute('disabled')){
      attr = 0;
      submitBtn.setAttribute('disabled','');
    } else {
      attr = 1;
    }
    this.formData = file;
    console.log("File grabbed " + this.formData);
    this.formData.append('who', 'admin');
    this._http.post(`${this.uploadUrl}`, this.formData).subscribe(
      res => {
        console.log(res);
        this.machine.data.image = res['secure_url'];
        this.photoLoader = false;

        if(attr==0){
          submitBtn.removeAttribute('disabled');
        }
      }, err => {
        console.log(err);
        if(attr==0){
          submitBtn.removeAttribute('disabled');
        }
        alert('Please, check network connection and refresh page!');
        this.networkError = true;        
      });
  }

  addMachinery() {
    this.loader = true
    this.machine.token = this.admin.token;
    console.log(this.machine);
    this._machineryService.postData(this.machineUrl, this.machine ).subscribe(res => {
      if (res['error'] == true) {
        alert(res['message']);
        this.loader = false;
        console.log(res);
      } else {
        console.log(res);
        alert(res['message']);
        this.loader = false;
        this.machine = {
          'data': {
            'name': '',
            'description': '',
            'location': '',
            'category': '',
            'image': '',
            'address': '',
            'contact_phone': '',
            'contact_email': '',
            'price': '',
            "classified": false
          },
          'token': ''
      
        }
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!')
      this.loader = false;
      this.networkError = true;      
    });

  }

}
