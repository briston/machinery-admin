import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMachineriesComponent } from './all-machineries.component';

describe('AllMachineriesComponent', () => {
  let component: AllMachineriesComponent;
  let fixture: ComponentFixture<AllMachineriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMachineriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMachineriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
