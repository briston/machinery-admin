import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-view-subscriptions',
  templateUrl: './view-subscriptions.component.html',
  styleUrls: ['./view-subscriptions.component.css']
})
export class ViewSubscriptionsComponent implements OnInit {
  admin: any;
  subscriptions: any;
  pageTitle: string = 'View Subscriptions';
  subscriptionUrl: string = 'subscriptions';
  deleteSubscriptionUrl: string = 'subscription';
  loader: boolean = true;
  networkError: boolean = false;
  activePage:string;

  constructor(private _machineryService: MachineriesService, private router: Router) { }

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.activePage = 'subs';
    this.getSubscriptions();
  }

  getSubscriptions() {
    this._machineryService.getData(this.subscriptionUrl, `?token=${this.admin.token}&page=0&order=-updated_at`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
      } else {
        this.subscriptions = res['result'];
        console.log("Adverts: ", this.subscriptions);
        this.loader = false;
      }
    }, err => {
      console.log(err);
      this.loader = false;
      this.networkError = true;
      alert('Please, check network connection and refresh page!');
    });
  }

  viewSubscription(id){
    window.location.href = `/view-subscription/${id}`;
  }

  deleteSubscription(id) {
    let data = {
      'data': {
        '_id': id
      },
      'token': this.admin.token
    }
    console.log(data);
    if (confirm('Are you sure you want to delete this Package?')) {
      this._machineryService.deleteData(this.deleteSubscriptionUrl, data).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          console.log(res);
        } else {
          alert(res['message']);
          this.getSubscriptions();
          console.log(res);
        }
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
      });
    } else {
      console.log('No');
    }

  }
}
