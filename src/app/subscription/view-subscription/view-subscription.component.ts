import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-view-subscription',
  templateUrl: './view-subscription.component.html',
  styleUrls: ['./view-subscription.component.css']
})
export class ViewSubscriptionComponent implements OnInit {
  loader_2: boolean;
  networkError: boolean = false;
  pageTitle: string;
  subscriptionUrl: string = 'subscription';
  subscription = {
    "data": {
      "subscription_package": "",
      "price": "",
      "status": false as boolean,
      "duration": "",
      "feature": {},
      "level": ""
    },
    'find': {
      '_id': ''
    },
    "token": ''
  };

  admin;
  loader: boolean = false;
  sub;
  id;
  //set input field to focus
  input_focus = 'is-focused';
  activePage: string;

  constructor(private _machineryService: MachineriesService, private _http: HttpClient,
    private activtedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.pageTitle = 'Edit Subscription';
    this.activePage = 'subs';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.sub = this.activtedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getSubscription();
  }

  getSubscription() {
    let id = JSON.stringify({
      '_id': this.id
    });
    console.log(id);
    this._machineryService.getData(this.subscriptionUrl, `?token=${this.admin.token}&data=${id}`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
        this.networkError = false;
      } else {
        console.log(res);
        this.subscription.data = res['result'];
        this.loader = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader = false;
      this.networkError = true;
    });
  }

  updateSubscription() {
    this.loader = true;
    this.loader_2 = true;
    this.subscription.find._id = this.id;
    this.subscription.token = this.admin.token;
    console.log(this.subscription);
    if (this.subscription.data.level == "") {
      alert('Please, level is required');
      this.loader = false;
    } else {
      this._machineryService.putData(this.subscriptionUrl, this.subscription).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          this.loader = false;
          this.loader_2 = false;
          console.log(res);
        } else {
          console.log(res);
          alert(res['message']);
          this.loader = false;
          this.loader_2 = false;
        }
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!')
        this.loader = false;
        this.networkError = true;
      });
    }

  }

  //set input field to focus
  ngOnChanges() {
    this.input_focus = 'is-focused';
    console.log('focused');
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
