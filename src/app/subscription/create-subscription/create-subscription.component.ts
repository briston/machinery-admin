import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-create-subscription',
  templateUrl: './create-subscription.component.html',
  styleUrls: ['./create-subscription.component.css']
})
export class CreateSubscriptionComponent implements OnInit {
  subscriptionUrl: string = 'subscription';
  activePage: string;
  pageTitle: any;
  subscription = {
    "data": {
      "subscription_package": "",
      "price": "",
      "status": false as boolean,
      "duration": "",
      "feature": {},
      "level": "",
    },
    "token": ''
  };
  admin;
  loader: boolean = false;

  constructor(private _machineryService: MachineriesService, private _http: HttpClient) { }

  ngOnInit() {
    this.activePage = 'create-sub';
    this.pageTitle = 'Create Subscription';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
  }

  createSubscription() {
    this.loader = true;
    this.subscription.token = this.admin.token;
    console.log('Subscription: ', this.subscription);
    if (this.subscription.data.level == "") {
      alert('Please, level is required')
      this.loader = false;
    } else {
      this._machineryService.postData(this.subscriptionUrl, this.subscription).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          this.loader = false;
          console.log(res);
        } else {
          alert(res['message']);
          console.log('Subscriptions res: ', res);
          this.loader = false;
          this.subscription = {
            "data": {
              "subscription_package": "",
              "price": "",
              "status": false as boolean,
              "duration": "",
              "feature": {},
              "level": "",
            },
            "token": ''
          };
        }
      }, err => {
        alert('Please, check network connection and refresh page!');
        this.loader = true;
        console.log(err);
      });
    }
  }

}
