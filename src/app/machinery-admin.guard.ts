import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MachineryAdminGuard implements CanActivate {

  constructor(private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(!localStorage.getItem('admin') || localStorage.getItem('admin')==null){
        alert('Please, login to view page!');
        this.router.navigateByUrl('login');
        return false;
      } else {
        return true;
      }
    
  }
}
