import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MachineriesService } from './machineries.service';
import { Routes } from './machineries.routes';
import { BaloModule } from 'balo_modules';
import { MachineryAdminGuard } from './machinery-admin.guard';


import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AllMachineriesComponent } from './machineries/all-machineries/all-machineries.component';
import { AddMachineryComponent } from './machineries/add-machinery/add-machinery.component';
import { LoginComponent } from './login/login.component';
import { MachineryComponent } from './machineries/machinery/machinery.component';
import { RegisterComponent } from './register/register.component';
import { ViewOwnersComponent } from './owners/view-owners/view-owners.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { ProfileComponent } from './owners/profile/profile.component';
import { ViewCategoriesComponent } from './category/view-categories/view-categories.component';
import { ViewCategoryComponent } from './category/view-category/view-category.component';
import { CreateAdvertComponent } from './advert/create-advert/create-advert.component';
import { ViewAdvertComponent } from './advert/view-advert/view-advert.component';
import { ViewAdvertsComponent } from './advert/view-adverts/view-adverts.component';
import { CreateSubscriptionComponent } from './subscription/create-subscription/create-subscription.component';
import { ViewSubscriptionComponent } from './subscription/view-subscription/view-subscription.component';
import { ViewSubscriptionsComponent } from './subscription/view-subscriptions/view-subscriptions.component';
import { CreateClassifiedComponent } from './classified/create-classified/create-classified.component';
import { AllClassifiedsComponent } from './classified/all-classifieds/all-classifieds.component';
import { ClassifiedComponent } from './classified/classified/classified.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    AdminProfileComponent,
    AllMachineriesComponent,
    AddMachineryComponent,
    LoginComponent,
    MachineryComponent,
    RegisterComponent,
    ViewOwnersComponent,
    AddCategoryComponent,
    ProfileComponent,
    ViewCategoriesComponent,
    ViewCategoryComponent,
    CreateAdvertComponent,
    ViewAdvertComponent,
    ViewAdvertsComponent,
    CreateSubscriptionComponent,
    ViewSubscriptionComponent,
    ViewSubscriptionsComponent,
    CreateClassifiedComponent,
    AllClassifiedsComponent,
    ClassifiedComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(Routes),
    FormsModule,
    HttpClientModule,
    BaloModule
  ],
  providers: [
    MachineriesService,
    MachineryAdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
