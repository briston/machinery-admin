import { Component, OnInit } from '@angular/core';
import { MachineriesService } from '../../machineries.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
  categoryUrl:string = 'category';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';

  category = {
    'data': {
      'category_name': '',
      'about': '',
      'status': true,
      'image': ''
    },
    'token': ''   
  }

  networkError: boolean = false;
  pageTitle: string;
  admin;
  loader:boolean = false;
  activePage:string;
  formData = new FormData();
  photoLoader = false;

  constructor(private _machineryService: MachineriesService, private _http: HttpClient) { }

  ngOnInit() {
    this.pageTitle = 'Add Category';
    this.activePage = 'add-category';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;    
  }


  addCategory() {
    this.loader = true;    
    this.category.token = this.admin.token;
    console.log(this.category);
    this._machineryService.postData(this.categoryUrl, this.category ).subscribe(res => {
      if (res['error'] == true) {
        alert(res['message']);
        this.loader = false;
        console.log(res);
      } else {
        console.log(res);
        alert(res['message']);
        this.loader = false;
        this.category = {
          'data': {
            'category_name': '',
            'about': '',
            'status': true,
            'image': ''
          },
          'token': ''   
        }
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!')
      this.loader = false;      
      this.networkError = true;      
    });

  }

  // Grab photo file
  grabPhoto = (file) => {
    this.photoLoader = true;
    // set disable to submit botton on photo upload
    let attr:number;
    let submitBtn = document.getElementById('submitBtn');
    if(!submitBtn.hasAttribute('disabled')){
      attr = 0;
      submitBtn.setAttribute('disabled','');
    } else {
      attr = 1;
    }
    this.formData = file;
    console.log("File grabbed " + this.formData);
    this.formData.append('who', 'admin');
    this._http.post(`${this.uploadUrl}`, this.formData).subscribe(
      res => {
        console.log(res);
        this.category.data.image = res['secure_url'];
        this.photoLoader = false;

        if(attr==0){
          submitBtn.removeAttribute('disabled');
        }
      }, err => {
        console.log(err);
        if(attr==0){
          submitBtn.removeAttribute('disabled');
        }
        alert('Please, check network connection and refresh page!');
        this.networkError = true;        
      });
  }

}
