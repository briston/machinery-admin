import { Component, OnInit } from '@angular/core';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-view-categories',
  templateUrl: './view-categories.component.html',
  styleUrls: ['./view-categories.component.css']
})
export class ViewCategoriesComponent implements OnInit {
  pageTitle:string;
  categoryUrl:string = 'categories';
  deleteCategoryUrl:string = 'category';  
  categories;
  admin
  searchData:string;
  loader:boolean = true;
  networkError: boolean = false;
  activePage:string;

  constructor(private _machineryService: MachineriesService) { }

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.pageTitle = 'All Categories';
    this.activePage = 'categories';
    this.getCategories();
  }

  getCategories(){
    this._machineryService.getData(this.categoryUrl, `?token=${this.admin.token}&page=0&order=-updated_at` ).subscribe(res=> {
      if(res['error']==true){
        console.log(res);
        this.loader = false;
      } else {
        console.log(res);
        this.categories = res['result'];
        this.loader = false;
      }
    }, err=>{
      alert('Please, check network connection and refresh page!')
      this.loader = false;
      this.networkError = true;
      console.log(err);
    });
  }

  getCategory(id){
    window.location.href=`/view-category/${id}`;
  }

  deleteCategory(id) {
    let data = {
      'data': {
        '_id': id
      },
      'token': this.admin.token
    }
    console.log(data);
    if (confirm('Are you sure you want to delete this Machine?')) {
      this._machineryService.deleteData(this.deleteCategoryUrl, data).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          console.log(res);
        } else {
          alert(res['message']);
          this.getCategories();
          console.log(res);
        }
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
      });
    } else {
      console.log('No');
    }

  }

}
