import { Component, OnInit, OnDestroy } from '@angular/core';
import { MachineriesService } from '../../machineries.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.css']
})
export class ViewCategoryComponent implements OnInit, OnDestroy {
  categoryUrl:string = 'category';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';

  category = {
    'data': {
      'category_name': '',
      'about': '',
      'status': true,
      'image': ''
    },
    'find': {
      '_id': ''
    },
    'token': ''   
  }

  networkError: boolean = false;
  pageTitle: string;
  admin;
  loader:boolean = false;
  loader_2: boolean = false;
  sub;
  id;
  activePage:string;
  formData = new FormData();
  photoLoader = false;
  is_focused = 'is-focused';

  constructor(private _machineryService: MachineriesService, private activtedRoute: ActivatedRoute, private _http: HttpClient) { }

  ngOnInit() {
    this.pageTitle = 'View Category';
    this.activePage = 'categories';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;    
    this.sub = this.activtedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getCategory();
  }

  getCategory() {
    let id = JSON.stringify({
      '_id': this.id
    });
    console.log(id);
    this._machineryService.getData(this.categoryUrl, `?token=${this.admin.token}&data=${id}`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
        this.networkError = false;
      } else {
        console.log(res);
        this.category.data = res['result'];
        this.category.find._id = res['result']['_id'];
        this.loader = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader = false;
      this.networkError = true;
    });
  }


  updateCategory() {
    this.loader_2 = true;    
    this.category.token = this.admin.token;
    console.log(this.category);
    this._machineryService.putData(this.categoryUrl, this.category ).subscribe(res => {
      if (res['error'] == true) {
        alert(res['message']);
        this.loader_2 = false;
        console.log(res);
      } else {
        console.log(res);
        alert(res['message']);
        this.loader_2 = false;
        // this.category = {
        //   'data': {
        //     'category_name': '',
        //     'about': '',
        //     'status': true
        //   },
        //   'token': ''   
        // }
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!')
      this.loader = false;      
      this.networkError = true;      
    });

  }
  // Grab photo file
  grabPhoto = (file) => {
    this.photoLoader = true;
    // set disable to submit botton on photo upload
    let attr:number;
    let submitBtn = document.getElementById('submitBtn');
    if(!submitBtn.hasAttribute('disabled')){
      attr = 0;
      submitBtn.setAttribute('disabled','');
    } else {
      attr = 1;
    }
    this.formData = file;
    console.log("File grabbed " + this.formData);
    this.formData.append('who', 'admin');
    this._http.post(`${this.uploadUrl}`, this.formData).subscribe(
      res => {
        console.log(res);
        this.category.data.image = res['secure_url'];
        this.photoLoader = false;

        if(attr==0){
          submitBtn.removeAttribute('disabled');
        }
      }, err => {
        console.log(err);
        if(attr==0){
          submitBtn.removeAttribute('disabled');
        }
        alert('Please, check network connection and refresh page!');
        this.networkError = true;        
      });
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

}
