import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MachineriesService } from '../machineries.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  admin: JSON;
  registerUrl:string = 'register';
  page;
  loader:boolean = false;

  adminReg = {
    'data': {
      'fname': '',
      'lname': '',
      'email': '',
      'password': '',
      'phone': '',
      'access_level': ''
    },
    'find' : {
      'email': ''
    },
    'token': ''
  };

  constructor(private _machineryService: MachineriesService, private router: Router) { }

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem('admin'))['result'];
    console.log(this.admin['token']);
  }

  register(){
    this.loader = true;
    this.adminReg.find.email = this.adminReg.data.email;
    this.adminReg.token = this.admin['token'];
    console.log(this.admin);
    this._machineryService.postData(this.registerUrl, this.adminReg).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        alert(res['message']);
        this.loader = false;
      } else {
        console.log(res);
        this.loader = false;
        localStorage.setItem('admin', JSON.stringify(res));
        this.router.navigateByUrl('/dashboard');
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page');
      this.loader = false;
    });
  }


}
