import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-view-adverts',
  templateUrl: './view-adverts.component.html',
  styleUrls: ['./view-adverts.component.css']
})
export class ViewAdvertsComponent implements OnInit {
  admin: any;
  adverts: any;
  pageTitle: string = 'View Adverts';
  advertUrl: string = 'classifieds';
  deleteAdvertUrl: string = 'advert';
  loader: boolean = true;
  networkError: boolean = false;
  activePage:string;

  constructor(private _machineryService: MachineriesService, private router: Router) { }

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.activePage = 'adverts';
    this.getAdverts();
  }

  getAdverts() {
    this._machineryService.getData(this.advertUrl, `?token=${this.admin.token}&page=0&order=-updated_at`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
      } else {
        this.adverts = res['result'];
        console.log("Adverts: ", this.adverts);
        this.loader = false;
      }
    }, err => {
      console.log(err);
      this.loader = false;
      this.networkError = true;
      alert('Please, check network connection and refresh page!');
    });
  }

  viewAdvert(id){
    window.location.href = `/view-advert/${id}`;
  }

  deleteAdvert(id) {
    let data = {
      'data': {
        '_id': id
      },
      'token': this.admin.token
    }
    console.log(data);
    if (confirm('Are you sure you want to delete this Owner?')) {
      this._machineryService.deleteData(this.deleteAdvertUrl, data).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          console.log(res);
        } else {
          alert(res['message']);
          this.getAdverts();
          console.log(res);
        }
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
      });
    } else {
      console.log('No');
    }

  }


}
