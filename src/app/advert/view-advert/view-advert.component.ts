import { Component, OnInit, OnChanges } from '@angular/core';
import { MachineriesService } from '../../machineries.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-advert',
  templateUrl: './view-advert.component.html',
  styleUrls: ['./view-advert.component.css']
})
export class ViewAdvertComponent implements OnInit, OnChanges {
  loader_2: boolean;
  networkError: boolean = false;
  pageTitle: string;
  advertUrl: string = 'classified';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';
  formData = new FormData();
  photoLoader = false;

  advert = {
    'data': {
      'advert_name': '',
      'advert_area': '',
      'advert_image': '',
      'sponsor': '',
      'status': null as boolean
    },
    'find': {
      '_id': ''
    },
    'token': ''

  }

  admin;
  loader:boolean = false;
  sub;
  id;
  input_focus = 'is-focused';
  activePage:string;

  constructor(private _machineryService: MachineriesService, private _http: HttpClient,
    private activtedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.pageTitle = 'Edit Advert';
    this.activePage = 'adverts';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.sub = this.activtedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getAdvert();
  }

  getAdvert() {
    let id = JSON.stringify({
      '_id':this.id
    });
    console.log(id);
    this._machineryService.getData(this.advertUrl, `?token=${this.admin.token}&data=${id}`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
        this.networkError = false;
      } else {
        console.log(res);
        this.advert.data = res['result'];
        this.loader = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader = false;
      this.networkError = true;
    });
  }


  // Grab photo file
  grabPhoto = (file) => {
    this.photoLoader = true;
    this.loader_2 = true;                
    this.formData = file;
    console.log("File grabbed " + this.formData);
    this.formData.append('who', 'admin');
    this._http.post(`${this.uploadUrl}`, this.formData).subscribe(
      res => {
        console.log(res);
        this.advert.data.advert_image = res['secure_url'];
        this.photoLoader = false;
        this.loader_2 = false;        
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
        this.networkError = true;
        this.loader_2 = false; 
      });
  }

  updateAdvert() {
    this.loader = true;
    this.loader_2 = true;        
    this.advert.find._id = this.id;    
    this.advert.token = this.admin.token;
    console.log(this.advert);
    this._machineryService.putData(this.advertUrl, this.advert ).subscribe(res => {
      if (res['error'] == true) {
        alert(res['message']);
        this.loader = false;
        this.loader_2 = false;        
        console.log(res);
      } else {
        console.log(res);
        alert(res['message']);
        this.loader = false;
        this.loader_2 = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!')
      this.loader = false;
      this.networkError = true;      
    });

  }
  ngOnChanges(){
    this.input_focus = 'is-focused';
    console.log('focused');
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

}
