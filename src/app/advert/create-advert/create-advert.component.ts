import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-create-advert',
  templateUrl: './create-advert.component.html',
  styleUrls: ['./create-advert.component.css']
})
export class CreateAdvertComponent implements OnInit {
  loader_2: boolean;
  networkError: boolean = false;
  pageTitle: string;
  machineUrl: string = 'classified';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';
  formData = new FormData();
  photoLoader = false;

  advert = {
    'data': {
      'advert_name': '',
      'advert_area': '',
      'advert_image': '',
      'sponsor': '',
      'status': null as boolean
    },
    'token': ''

  }

  admin;
  loader:boolean = false;
  activePage:string;

  constructor(private _machineryService: MachineriesService, private _http: HttpClient) { }

  ngOnInit() {
    this.pageTitle = 'Create Advert';
    this.activePage = 'add-advert';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;    
  }

  // Grab photo file
  grabPhoto = (file) => {
    this.photoLoader = true;
    this.loader_2 = true;                
    this.formData = file;
    console.log("File grabbed " + this.formData);
    this.formData.append('who', 'admin');
    this._http.post(`${this.uploadUrl}`, this.formData).subscribe(
      res => {
        console.log(res);
        this.advert.data.advert_image = res['secure_url'];
        this.photoLoader = false;
        this.loader_2 = false;        
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
        this.networkError = true;
        this.loader_2 = false; 
      });
  }

  createAdvert() {
    this.loader = true;
    this.loader_2 = true;            
    this.advert.token = this.admin.token;
    console.log(this.advert);
    this._machineryService.postData(this.machineUrl, this.advert ).subscribe(res => {
      if (res['error'] == true) {
        alert(res['message']);
        this.loader = false;
        this.loader_2 = false;        
        console.log(res);
      } else {
        console.log(res);
        alert(res['message']);
        this.loader = false;
        this.loader_2 = false;        
        this.advert = {
          'data': {
            'advert_name': '',
            'advert_area': '',
            'advert_image': '',
            'sponsor': '',
            'status': null as boolean
          },
          'token': ''
      
        }
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!')
      this.loader = false;
      this.networkError = true;      
    });

  }


}
