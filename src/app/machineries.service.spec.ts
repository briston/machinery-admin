import { TestBed, inject } from '@angular/core/testing';

import { MachineriesService } from './machineries.service';

describe('MachineriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MachineriesService]
    });
  });

  it('should be created', inject([MachineriesService], (service: MachineriesService) => {
    expect(service).toBeTruthy();
  }));
});
