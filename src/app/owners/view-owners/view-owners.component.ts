import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-view-owners',
  templateUrl: './view-owners.component.html',
  styleUrls: ['./view-owners.component.css']
})
export class ViewOwnersComponent implements OnInit {
  pageTitle: string = 'View Machine Owners';
  activePage: string;
  machineOwnersUrl: string = 'machine-owners';
  deleteMachineOwnersUrl: string = 'machine-owner';
  loader: boolean = true;
  networkError: boolean = false;
  page:number = 0;
  disableNext: boolean = false;
  disablePrev: boolean = false;
  admin: any;
  machineryOwners: any;

  constructor(private _machineryService: MachineriesService, private router: Router) { }

  ngOnInit() {
    this.activePage = 'owners';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.getMachineOwners();
  }

  getMachineOwners(page?) {
    console.log('page: ',this.page);
     //Manage prev pagination
    if(page >= 0){
      this.page = page;
    }
    if(this.page==0){
      this.disablePrev = true;
    } else{
      this.disablePrev = false;
    }
    this._machineryService.getData(this.machineOwnersUrl, `?token=${this.admin.token}&page=${this.page}&limit=20&order=-updated_at`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
      } else {
        this.machineryOwners = res['result'];
        //manage next pagination
        if(this.machineryOwners.length == 0 || this.machineryOwners.length < 20){
          this.disableNext = true;
          if(this.page != 0) {
            this.disablePrev = false;
          }
        } else{
          this.disableNext = false;
        }
        console.log("Machine Owners: ", this.machineryOwners);
        this.loader = false;
      }
    }, err => {
      console.log(err);
      this.loader = false;
      this.networkError = true;
      alert('Please, check network connection and refresh page!');
    });
  }

  viewMachineOwner(id){
    window.location.href = `/profile/${id}`;
  }

  deleteMachineOwner(id) {
    let data = {
      'data': {
        '_id': id
      },
      'token': this.admin.token
    }
    console.log(data);
    if (confirm('Are you sure you want to delete this Owner?')) {
      this._machineryService.deleteData(this.deleteMachineOwnersUrl, data).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          console.log(res);
        } else {
          alert(res['message']);
          this.getMachineOwners();
          console.log(res);
        }
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
      });
    } else {
      console.log('No');
    }

  }

}
