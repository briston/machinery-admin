import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MachineriesService } from '../../machineries.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loader_2: boolean;
  pageTitle: string;
  activePage: string;
  machineOwnerUrl: string = 'machine-owner';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';
  formData = new FormData();
  photoLoader = false;
  machineryOwner = {
    'company_name': '',
    'phone': '',
    'email': '',
    'address': '',
    'location': '',
    'status': null as boolean,
    'verified': null as boolean
  };
  admin
  sub;
  id;

  loader: boolean = true;
  networkError: boolean = false;

  
  constructor(private _machineService: MachineriesService, private activtedRoute: ActivatedRoute,
    private _http: HttpClient) { }

  ngOnInit() {
    this.pageTitle = 'Owner Profile';
    this.activePage = 'owners';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.sub = this.activtedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getMachineryOwner();
  }

  getMachineryOwner() {
    let id = JSON.stringify({
      '_id': this.id
    });
    console.log(id);
    console.log('url: ',`?token=${this.admin.token}&data=${id}`)
    this._machineService.getData(this.machineOwnerUrl, `?token=${this.admin.token}&data=${id}`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
        this.networkError = false;
      } else {
        console.log(res);
        this.machineryOwner = res['result'];
        this.loader = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader = false;
      this.networkError = true;
    });
  }

  updateMachineOwner(){
    console.log(this.machineryOwner);
    this.loader_2 = true;
    let data = {
      'data': this.machineryOwner,
      'find': { '_id': this.id },
      'token': this.admin.token
    }
    console.log('update data: ', data);
    this._machineService.putData(this.machineOwnerUrl, data).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader_2 = false;
        this.networkError = false;
      } else {
        console.log(res);
        alert(res['message']);
        this.getMachineryOwner();
        this.loader_2 = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader_2 = false;
      this.networkError = true;
    });
  }
}
