import { Component, OnInit } from '@angular/core';
import { MachineriesService } from '../machineries.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  pageTitle:string;
  activePage:string;
  machineUrl:string = 'machines';
  machineOwnersUrl:string = 'machine-owners';
  machineries;
  machineryOwners: any;
  admin
  loader:boolean = true;
  networkError: boolean = false;

  constructor(private _machineryService: MachineriesService, private router: Router) { }

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.pageTitle = 'Dashboard';
    this.activePage = 'dashboard';
    this.getMachineries();
    this.getMachineOwners();
  }

  getMachineries(){
    this._machineryService.getData(this.machineUrl, `?token=${this.admin.token}&page=0&limit=3&order=-updated_at`).subscribe(res=> {
      if(res['error']==true){
        console.log(res);
        this.loader = false;
      } else {
        this.machineries = res['result'];
        console.log("Machines: ",this.machineries);
        this.loader = false;
      }
    }, err=>{
      console.log(err);
      this.loader = false;
      this.networkError = true;
      alert('Please, check network connection and refresh page!');
    });
  }

  getMachineOwners(){
    this._machineryService.getData(this.machineOwnersUrl, `?token=${this.admin.token}&page=0&limit=5&order=-updated_at`).subscribe(res=> {
      if(res['error']==true){
        console.log(res);
      } else {
        this.machineryOwners = res['result'];
        console.log("Machinery Owners: ",this.machineryOwners);
      }
    }, err=>{
      console.log(err);
    });
  }

  getMachinery(id){
    window.location.href=`/machine/${id}`;
  }

  viewMachineOwner(id){
    window.location.href = `/profile/${id}`;
  }

}
