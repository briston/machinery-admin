import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllClassifiedsComponent } from './all-classifieds.component';

describe('AllClassifiedsComponent', () => {
  let component: AllClassifiedsComponent;
  let fixture: ComponentFixture<AllClassifiedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllClassifiedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllClassifiedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
