import { Component, OnInit } from '@angular/core';
import { MachineriesService } from '../../machineries.service';

@Component({
  selector: 'app-all-classifieds',
  templateUrl: './all-classifieds.component.html',
  styleUrls: ['./all-classifieds.component.css']
})
export class AllClassifiedsComponent implements OnInit {
  loader_2: boolean;
  searchUrl:string = 'search-machines';
  pageTitle:string;
  machineUrl:string = 'machines';
  deleteMachineUrl:string = 'machine';  
  machineries;
  admin
  searchData:string;
  loader:boolean = true;
  networkError: boolean = false;
  page:number = 0;
  disableNext: boolean = false;
  disablePrev: boolean = false;
  activePage:string;

  constructor(private _machineryService: MachineriesService) { }

  ngOnInit() {
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.pageTitle = 'All Classifieds';
    this.activePage = 'classifieds';
    this.getMachineries();
  }

  getMachineries(page?){
    //Manage prev pagination
    if(page >= 0){
      this.page = page;
    }
    if(this.page==0){
      this.disablePrev = true;
    } else{
      this.disablePrev = false;
    }
    this.searchData = JSON.parse(localStorage.getItem('search'));    
    let request;
    let url;
    //Manage search and default action
    if(this.searchData){
      //search
      request = `?search=${this.searchData}&token=${this.admin.token}&page=0&limit=20&order=-updated_at`
      url = this.searchUrl;
    } else {
      //default
      let classified = JSON.stringify({"classified":true});
      request = `?token=${this.admin.token}&data=${classified}&page=${this.page}&limit=20&order=-updated_at`
      url = this.machineUrl;
    }
    this._machineryService.getData(url, request ).subscribe(res=> {
      if(res['error']==true){
        console.log(res);
        this.loader = false;
      } else {
        console.log(res);
        this.machineries = res['result'];
        //manage next pagination
        if(this.machineries.length == 0 || this.machineries.length < 20){
          this.disableNext = true;
            if(this.page != 0) {
            this.disablePrev = false;
          }
        } else {
          this.disableNext = false;
        }
        this.loader = false;
        this.loader_2 = true;
        //Clear search data
        localStorage.removeItem('search');
      }
    }, err=>{
      alert('Please, check network connection and refresh page!')
      this.loader = false;
      this.networkError = true;
      console.log(err);
    });
  }

  getMachinery(id){
    window.location.href=`/classified/${id}`;
  }

  deleteMachine(id) {
    let data = {
      'data': {
        '_id': id
      },
      'token': this.admin.token
    }
    console.log(data);
    if (confirm('Are you sure you want to delete this Machine?')) {
      this._machineryService.deleteData(this.deleteMachineUrl, data).subscribe(res => {
        if (res['error'] == true) {
          alert(res['message']);
          console.log(res);
        } else {
          alert(res['message']);
          this.getMachineries();
          console.log(res);
        }
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
      });
    } else {
      console.log('No');
    }

  }

}
