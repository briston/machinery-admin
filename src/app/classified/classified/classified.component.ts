import { Component, OnInit } from '@angular/core';
import { MachineriesService } from '../../machineries.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {
  loader_2: boolean;
  pageTitle: string;
  machineUrl: string = 'machine';
  categoriesUrl: string = 'categories';
  uploadUrl: string = 'https://machinery-ng.herokuapp.com/upload/upload';
  formData = new FormData();
  photoLoader = false;
  machinery;
  admin
  sub;
  id;
  activePage:string;

  categories: any = [];

  loader: boolean = true;
  networkError: boolean = false;
  is_focused = 'is-focused';

  constructor(private _machineryService: MachineriesService, private activtedRoute: ActivatedRoute,
    private _http: HttpClient) { }

  ngOnInit() {
    this.pageTitle = 'View Classified';
    this.activePage = 'classifieds';
    this.admin = JSON.parse(localStorage.getItem('admin')).result;
    this.sub = this.activtedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getMachinery();
  }

  getCategories(){
    let token = this.admin['token'];
    this._machineryService.getData(this.categoriesUrl, `?token=${token}&page=0&order=-updated_at` ).subscribe(res=> {
      if(res['error']==true){
        console.log(res);
        // this.loader = false;
      } else {
        console.log(res);
        this.categories = res['result'];
        // this.loader = false;
      }
    }, err=>{
      alert('Please, check network connection and refresh page!')
      // this.loader = false;
      this.networkError = true;
      console.log(err);
    });
  }

  getMachinery() {
    let id = JSON.stringify({
      '_id': this.id,
    });
    let classified = JSON.stringify({"classified":false});
    console.log(id);
    this._machineryService.getData(this.machineUrl, `?token=${this.admin.token}&data=${id}`).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader = false;
        this.networkError = false;
      } else {
        console.log(res);
        this.machinery = res['result'];
        this.getCategories();
        this.loader = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader = false;
      this.networkError = true;
    });
  }

  // Grab photo file
  grabPhoto = (file) => {
    this.photoLoader = true;
    this.formData = file;
    console.log("File grabbed " + this.formData);
    this.formData.append('who', 'admin');
    this._http.post(`${this.uploadUrl}`, this.formData).subscribe(
      res => {
        console.log(res);
        this.machinery.image = res['secure_url'];
        this.photoLoader = false;
      }, err => {
        console.log(err);
        alert('Please, check network connection and refresh page!');
        this.photoLoader = false;
        this.networkError = true;
      });
  }

  updateMachinery() {
    this.loader_2 = true;
    let data = {
      'data': this.machinery,
      'find': { '_id': this.id },
      'token': this.admin.token
    }
    console.log('update data: ', data);
    this._machineryService.putData(this.machineUrl, data).subscribe(res => {
      if (res['error'] == true) {
        console.log(res);
        this.loader_2 = false;
        this.networkError = false;
      } else {
        console.log(res);
        alert(res['message']);
        this.getMachinery();
        this.loader_2 = false;
      }
    }, err => {
      console.log(err);
      alert('Please, check network connection and refresh page!');
      this.loader_2 = false;
      this.networkError = true;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
