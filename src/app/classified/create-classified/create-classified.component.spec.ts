import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateClassifiedComponent } from './create-classified.component';

describe('CreateClassifiedComponent', () => {
  let component: CreateClassifiedComponent;
  let fixture: ComponentFixture<CreateClassifiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateClassifiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateClassifiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
